﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstRepApp.Controllers
{
    public class FirstRepController : Controller
    {
        //
        // GET: /FirstRep/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /FirstRep/Welcome
        public ActionResult Welcome(string name, int numTimes = 1)
        {
            ViewBag.Message = string.Format("Привет {0}", name);
            ViewBag.NumTimes = numTimes;

            return View();
        }
	}
}