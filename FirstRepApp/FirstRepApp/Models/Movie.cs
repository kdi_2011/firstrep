﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace FirstRepApp.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Display(Name = "Наименование")]
        [StringLength(60, MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string Title { get; set; }

        [Display(Name = "Дата выхода")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }

        [Display(Name = "Жанр")]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [Required]
        [StringLength(30)]
        [DataType(DataType.Text)]
        public string Genre { get; set; }

        [Range(1, 100)]
        [DataType(DataType.Currency)]
        [Display(Name = "Стоимость")]         
        public decimal Price { get; set; }

        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [StringLength(5)]
        [Display(Name = "Рейтинг")]
        [DataType(DataType.Text)]
        public string Rating { get; set; }

    }

    public class MovieDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
    }
}