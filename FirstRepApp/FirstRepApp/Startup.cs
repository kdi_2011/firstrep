﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FirstRepApp.Startup))]
namespace FirstRepApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
